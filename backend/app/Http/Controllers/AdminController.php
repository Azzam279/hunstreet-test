<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Helpers\ResponseFormatter;
use App\Mail\InvitationMail;

class AdminController extends Controller
{
    public function sendInvitationLink(Request $request)
    {
        $randomChars     = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token           = substr(str_shuffle($randomChars), 0, 15);
        $objData         = new \stdClass();
        $objData->url    = route('home');
        $objData->token  = $token;
        $details['data'] = $objData;
        $email           = new InvitationMail($details['data']);

        try {
            Mail::to('an0ngh0st00001@gmail.com')->send($email);

            return ResponseFormatter::success([
                'message'         => 'Email berhasil terkirim!',
            ], 'Email Sent');
        } catch (\Exception $e) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error'   => $e,
            ], 'Failed to send email', 500);
        }
    }
}
